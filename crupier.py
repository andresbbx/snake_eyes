import random
from typing import Dict

from logger import logger


class Crupier:
    def __init__(self):

        self.tables = {}

    async def create_table(self, table_name):
        self.tables[table_name] = {
            "users": [],
        }

    async def get_table(self, table_name):
        return self.tables.get(table_name)

    async def set_table(self, table_name, table_info):
        self.tables[table_name] = table_info

    async def roll(self, dice: Dict):
        result = {}
        for key, value in dice.items():
            die_sides = int(key.strip('d'))
            result[key] = [
                random.randint(1,die_sides) for i in range(int(value))
            ]
        return result
